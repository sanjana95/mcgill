# Coders for Habitability

## Application
> + __Reads the fits files .__
> + __Plots the variation of flux of brightest pixel with time.__

## Instruction to run
                    python lightplot.py <NumOfAORkeys> <aorkey1> ... <aorkeyn>

## Directory structure
```.
+ -- lightplot.py
+ -- data
|   + -- <aorkey>
|   |   + -- ch2
|   |   |   + -- bcd
|   |   |   |   + --<image_files>
```