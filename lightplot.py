from __future__ import print_function
from astropy.io import fits
import numpy as np
import sys
from os import listdir
from os.path import isfile, join
import matplotlib.pyplot as plt
from scipy import stats
import time

class lightcurve():

    def __init__(self, aorkey):
        self.aorkey = aorkey
#        self.no_aorkey = len(self.aorkey)
        self.data_dir_path = "./data/"
        

    def get_bcd_files(self,aorkey):
        files = []
        aor_path = join(self.data_dir_path,self.aorkey,"ch2","bcd")
        try:
            for f in listdir(aor_path):
                if (isfile(join(aor_path,f)) and f.find("bcd")!=-1):
                    files.append(join(aor_path,f))
        except:
            print("The given directory is not valid")
            sys.exit(0)
        files = self.bcd_inorder(files)
        return files

    def get_images(self,files):
        datacubes = []
        for f in files:
            images = []
            try:
                hdulist = fits.open(f)
            except:
                print("Sorry there seems to be some error in the fits format for " +f+". For now I am ignoring this file")
              
            for img in hdulist[0].data:
                where_are_NaNs  =np.isnan(img)
                img[where_are_NaNs] = 0
                images.append(img)
            datacubes.append(images)
                
        return datacubes
        
    def bcd_inorder(self, files):
        self.bcd_bmjd = {}
        for f in files:
            try:
                hdulist = fits.open(f)
                self.bcd_bmjd[hdulist[0].header['BMJD_OBS']] = f
            except:
                print("Sorry there seems to be some error in the fits format for " +f+". For now I am ignoring this file")
                
        files_inorder=[]
        expid = self.bcd_bmjd.keys()
        expid.sort()
        for i in range(len(expid)):
            files_inorder.append(self.bcd_bmjd[expid[i]])
            
        return files_inorder

    def get_brightest_pixel(self,datacubes):
        bright = []
        for cube in datacubes:
            for img in cube:
                bright.append(np.unravel_index(img.argmax(),img.shape))
        elem, freq = stats.mode(bright)
        return elem[0]

    def get_pixel_value(self,datacubes):
        pixel_v = []
        for cube in datacubes:
            for img in cube:
                pixel_v.append(np.max(img))
        return pixel_v
        
    def get_timestamp(self):
        t = self.bcd_bmjd.keys()
        timestamp = []
        t.sort()
        interval = 100
        flag = 0
        temp = 0
        for time in t:
            if flag == 1:
                interval = min(interval, time - temp)
                temp = time
            else:
                temp = time
                flag = 1
        for time in t:
            for i in range(64):
                timestamp.append(time+(interval/64)*i)
        return timestamp
    
    def show_curve(self,x,y,aorkey):
        plt.figure(aorkey)    
        plt.plot(x,y)
        plt.ylabel("MJy/sr")
        plt.xlabel("[days] Solar System Barycenter Mod. Julian Date")
#        plt.show()

    
    def print_time_stats(self):
        print("Time get_bcd_files: ", self.end_get_bcd - self.start_get_bcd)
        print("Time get_images: ", self.end_get_image - self.end_get_bcd)
        print("Time get_brightest_pixel: " , self.end_get_bright - self.end_get_image)
        print("Time get_pixel_value: ", self.end_pix_val - self.end_get_bright)
        print("Time get_timestamp: ", self.end_timestamp - self.end_pix_val)
        
    def run(self):
        self.start_get_bcd = time.time()
        self.fits_files = self.get_bcd_files(aorkey)
        self.end_get_bcd = time.time()
 
        self.datacubes = self.get_images(self.fits_files)
        self.end_get_image = time.time()
            
        self.brightest_pixel = self.get_brightest_pixel(self.datacubes)
        self.end_get_bright = time.time()

        self.pixel_v = self.get_pixel_value(self.datacubes)
        self.end_pix_val = time.time()
        
        self.timestamp = self.get_timestamp()
        self.end_timestamp = time.time()
        
if __name__ == '__main__':
    no_aorkey = int(sys.argv[1])
    aorkey=[]
    for i in range(no_aorkey):
        aorkey.append(sys.argv[i+2])
    for i in range(len(aorkey)):
        print("aorkey",aorkey[i])
        l1 = lightcurve(aorkey[i])
        l1.run()
        l1.print_time_stats()
        l1.show_curve(l1.timestamp,l1.pixel_v,i)
    plt.show()
